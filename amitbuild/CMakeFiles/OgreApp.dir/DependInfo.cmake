# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/automata/ogre/clean-project/src/BaseApplication.cpp" "/home/automata/ogre/clean-project/amitbuild/CMakeFiles/OgreApp.dir/src/BaseApplication.cpp.o"
  "/home/automata/ogre/clean-project/src/TutorialApplication.cpp" "/home/automata/ogre/clean-project/amitbuild/CMakeFiles/OgreApp.dir/src/TutorialApplication.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "BOOST_ALL_NO_LIB"
  "BOOST_SYSTEM_NO_DEPRECATED"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/OIS"
  "/usr/local/include/OGRE"
  "/usr/local/include"
  "/usr/local/share/OGRE/Samples/Common/include"
  "/usr/local/include/OGRE/Overlay"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
