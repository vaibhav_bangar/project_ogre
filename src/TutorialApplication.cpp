/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _ 
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/                              
      Tutorial Framework
      http://www.ogre3d.org/tikiwiki/
-----------------------------------------------------------------------------
*/
#include "TutorialApplication.h"
#include <iostream>

//-------------------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//-------------------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//-------------------------------------------------------------------------------------

void TutorialApplication::createCamera(void)
{
    mCamera = mSceneMgr->createCamera("Newcam");
    mCamera->setPosition(Ogre::Vector3(0,300,500));
    mCamera->lookAt(0,0,0);
    mCamera->setNearClipDistance(5);

    mCameraMan = new OgreBites::SdkCameraMan(mCamera);
}

void TutorialApplication::createViewports(void)
{
  Ogre::Viewport* vp = mWindow->addViewport(mCamera);

  vp->setBackgroundColour(Ogre::ColourValue(0, 0, 0));

  mCamera->setAspectRatio(
    Ogre::Real(vp->getActualWidth()) /
    Ogre::Real(vp->getActualHeight()));
}

void TutorialApplication::createScene(void)
{
    // create your scene here :)
    // Set the scene's ambient light
    mSceneMgr->setAmbientLight(Ogre::ColourValue(0.5f, 0.5f, 0.5f));
    mSceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

//    mCamera->setPosition(0, 47,222);

//    mCamera->yaw(Ogre::Degree(180));
//    mCamera->pitch((Ogre::Degree(180)));

    const Ogre::Quaternion q = mCamera->getOrientation();
    printf("\n\nwassup123456: \n\n");
    std::cout << "Wassup" <<q<< q.getYaw() <<"\n\n\n";

    Ogre::Light* pointLight = mSceneMgr->createLight("PointLight");
     pointLight->setType(Ogre::Light::LT_POINT);
     pointLight->setPosition(250, 150, 250);
     pointLight->setDiffuseColour(Ogre::ColourValue::White);
     pointLight->setSpecularColour(Ogre::ColourValue::White);

     Ogre::Entity* ninjaEntity = mSceneMgr->createEntity("ninja.mesh");
     Ogre::SceneNode* ninjaNode = mSceneMgr->getRootSceneNode()->createChildSceneNode(
       "NinjaNode");
     ninjaNode->attachObject(ninjaEntity);

}

bool TutorialApplication::frameRenderingQueued(const Ogre::FrameEvent& fe)
{
  bool ret = BaseApplication::frameRenderingQueued(fe);

  if (!processUnbufferedInput(fe))
    return false;

  return ret;
}


bool TutorialApplication::processUnbufferedInput(const Ogre::FrameEvent& fe)
{
  static bool mouseDownLastFrame = false;
//  static Ogre::Real toggleTimer = 0.0;
  static Ogre::Real rotate = .13;
  static Ogre::Real move = 50;

//  // First toggle method
//  bool leftMouseDown = mMouse->getMouseState().buttonDown(OIS::MB_Left);

//  if (leftMouseDown && !mouseDownLastFrame)
//  {
//    Ogre::Light* light = mSceneMgr->getLight("PointLight");
//    light->setVisible(!light->isVisible());
//  }

//  mouseDownLastFrame = leftMouseDown;

//  // Second toggle method
//  toggleTimer -= fe.timeSinceLastFrame;

//  if ((toggleTimer < 0) && mMouse->getMouseState().buttonDown(OIS::MB_Right))
//  {
//    toggleTimer = .5;

//    Ogre::Light* light = mSceneMgr->getLight("PointLight");
//    light->setVisible(!light->isVisible());
//  }

  // Moving the Ninja
  Ogre::Vector3 dirVec = Ogre::Vector3::ZERO;

  if (mKeyboard->isKeyDown(OIS::KC_I))
    dirVec.z -= move;

  if (mKeyboard->isKeyDown(OIS::KC_K))
    dirVec.z += move;

  if (mKeyboard->isKeyDown(OIS::KC_U))
    dirVec.y += move;

  if (mKeyboard->isKeyDown(OIS::KC_O))
    dirVec.y -= move;

  if (mKeyboard->isKeyDown(OIS::KC_J))
  {
    if (mKeyboard->isKeyDown(OIS::KC_LSHIFT))
      mSceneMgr->getSceneNode("NinjaNode")->yaw(Ogre::Degree(3 * rotate));
    else
      dirVec.x -= move;
  }

  if (mKeyboard->isKeyDown(OIS::KC_L))
  {
    if (mKeyboard->isKeyDown(OIS::KC_LSHIFT))
      mSceneMgr->getSceneNode("NinjaNode")->yaw(Ogre::Degree(-3 * rotate));
    else
      dirVec.x += move;
  }

  mSceneMgr->getSceneNode("NinjaNode")->translate(
    dirVec * fe.timeSinceLastFrame,
    Ogre::Node::TS_LOCAL);

  return true;
}

    // Create an Entity
//    Ogre::Entity* ogreHead = mSceneMgr->createEntity("Head", "ninja.mesh");
//    Ogre::Entity* ogreHead2 = mSceneMgr->createEntity("ogrehead.mesh");
//    Ogre::Entity* ogreHead3 = mSceneMgr->createEntity("ogrehead.mesh");
//    Ogre::Entity* ogreHead4 = mSceneMgr->createEntity("ogrehead.mesh");

    // Create a SceneNode and attach the Entity to it
//    Ogre::SceneNode* headNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("HeadNode");
//    headNode->attachObject(ogreHead);

//    Ogre::Plane plane(Ogre::Vector3::UNIT_Y, 0);

//    Ogre::MeshManager::getSingleton().createPlane(
//     "ground",
//     Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
//     plane, 1500, 1500, 20, 20, true, 1, 5, 5, Ogre::Vector3::UNIT_Z);

//    Ogre::Entity* groundEntity = mSceneMgr->createEntity("ground");
//     mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(groundEntity);

//     groundEntity->setMaterialName("Examples/GreenSkin");
//       groundEntity->setCastShadows(false);

//       Ogre::Light* spotLight = mSceneMgr->createLight("Spotlight");
//        spotLight->setType(Ogre::Light::LT_SPOTLIGHT);

//        spotLight->setDiffuseColour(Ogre::ColourValue(0, 0, 1));
//        spotLight->setSpecularColour(Ogre::ColourValue(0, 0, 1));

//        spotLight->setDirection(-1, -1, 0);
//        spotLight->setPosition(Ogre::Vector3(200, 200, 0));

//        spotLight->setSpotlightRange(Ogre::Degree(35), Ogre::Degree(50));

//        // Directional light
//        Ogre::Light* directionalLight = mSceneMgr->createLight("DirectionalLight");
//        directionalLight->setType(Ogre::Light::LT_DIRECTIONAL);

//        directionalLight->setDiffuseColour(Ogre::ColourValue(.4, 0, 0));
//        directionalLight->setSpecularColour(Ogre::ColourValue(.4, 0, 0));

//        directionalLight->setDirection(Ogre::Vector3(0, -1, 1));

//        // Point light
//        Ogre::Light* pointLight = mSceneMgr->createLight("PointLight");
//        pointLight->setType(Ogre::Light::LT_POINT);

//        pointLight->setDiffuseColour(.3, .3, .3);
//        pointLight->setSpecularColour(.3, .3, .3);

//        pointLight->setPosition(Ogre::Vector3(0, 150, 250));


//    Ogre::SceneNode* Node = mSceneMgr->getRootSceneNode()->createChildSceneNode( Ogre::Vector3(84, 48, 0));
//    Node->roll(Ogre::Degree(90));
//    Node->attachObject(ogreHead2);


//    Ogre::SceneNode* Node1 = mSceneMgr->getRootSceneNode()->createChildSceneNode("Node1");
//    Node1->setPosition(Ogre::Vector3(0,104,0));
//    Node1->attachObject(ogreHead3);


//    Ogre::SceneNode* Node2 = mSceneMgr->getRootSceneNode()->createChildSceneNode("Node2");
//    Node2->setPosition(Ogre::Vector3(-84,48,0));
//    Node2->roll(Ogre::Degree(-90));
//    Node2->scale(1,1,1);
//    Node2->attachObject(ogreHead4);


    // Create a Light and set its position
//    Ogre::Light* light = mSceneMgr->createLight("MainLight");
//    light->setPosition(20.0f, 80.0f, 50.0f);



#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT )
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch( Ogre::Exception& e ) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox( NULL, e.getFullDescription().c_str(), "An exception has occured!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occured: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif
